﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Simulador_Carrera_de_Carros
{
    public partial class Form1 : Form
    {

        //Jhustin Ismael Arias Perez 4-A T/M

        Thread hilo;
        delegate void delegado(int valor);

        public Form1()
        {
            InitializeComponent();
        }

        //El boton carrera hace que los hilos trabajen y simulen la carrera

        private void Boton_Iniciar_Carrera_Click(object sender, EventArgs e)
        {
            hilo = new Thread(new ThreadStart(Corredor_1));
            hilo.Start();
            hilo = new Thread(new ThreadStart(Corredor_2));
            hilo.Start();
            hilo = new Thread(new ThreadStart(Corredor_3));
            hilo.Start();
        }

        //El delegado y el ciclo for de cada proceso me ayudaron con el objeto actualizar junto con el hilo

        #region "Proceso 1"
        public void Corredor_1()
        {
            for (int i = 0; i < 101; i++)
            {
                delegado MD = new delegado(Actualizar1);
                this.Invoke(MD, new object[] { i });
                Thread.Sleep(50);
            }
        }

        public void Actualizar1(int valor)
        {
            p1.Value = valor;
        }

        #endregion

        #region"Proceso 2"
        public void Corredor_2()
        {
            for (int i = 0; i < 101; i++)
            {
                delegado MD = new delegado(Actualizar2);
                this.Invoke(MD, new object[] { i });
                Thread.Sleep(70);
            }
        }

        public void Actualizar2(int valor)
        {
            p2.Value = valor;
        }

        #endregion

        #region "Proceso 3"
        public void Corredor_3()
        {
            for (int i = 0; i < 101; i++)
            {
                delegado MD = new delegado(Actualizar3);
                this.Invoke(MD, new object[] { i });
                Thread.Sleep(90);
            }
        }

        public void Actualizar3(int valor)
        {
            p3.Value = valor;
        }

        #endregion
    }
}
